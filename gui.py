import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from subscription_handler import SubscriptionHandler

class TreeView(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Edit Youtube Subscription feed")
        self.set_default_size(700, 500)

        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(True)
        self.grid.set_row_homogeneous(True)
        self.add(self.grid)

        self.sub_handler = SubscriptionHandler()

        self.subscription_list_store = Gtk.ListStore(str, str)

        self.sub_list_tree = Gtk.TreeView(model=self.subscription_list_store)
        self.renderer = Gtk.CellRendererText()
        self.channel_column = Gtk.TreeViewColumn("Channel name", self.renderer, text=0)
        self.id_column = Gtk.TreeViewColumn("Channel ID", self.renderer, text=1)
        self.sub_list_tree.append_column(self.channel_column)
        self.sub_list_tree.append_column(self.id_column)

        self.scrolled_list_subscriptions = Gtk.ScrolledWindow()
        self.scrolled_list_subscriptions.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.scrolled_list_subscriptions.add(self.sub_list_tree)

        self.add_subscription = Gtk.Button(label="Add")
        self.import_subscriptions = Gtk.Button(label="Import")
        self.export_subscriptions = Gtk.Button(label="Export")
        self.remove_subscription = Gtk.Button(label="Remove")
        self.channel_id_entry = Gtk.Entry()
        self.channel_id_entry.set_placeholder_text("Channel ID")

        #self.grid.add(self.scrolled_list_subscriptions)
        self.grid.attach(self.scrolled_list_subscriptions, 0, 0, 8, 10)
        self.grid.attach(self.remove_subscription, 0, 10, 1, 1)
        self.grid.attach(self.import_subscriptions, 1, 10, 1, 1)
        self.grid.attach(self.channel_id_entry, 2, 10, 4, 1)
        self.grid.attach(self.add_subscription, 6, 10, 1, 1)
        self.grid.attach(self.export_subscriptions, 7, 10, 1, 1)

        self.select = self.sub_list_tree.get_selection()

        self.add_subscription.connect("clicked", self.on_add_button_clicked)
        self.export_subscriptions.connect("clicked", self.on_export_button_clicked)
        self.import_subscriptions.connect("clicked", self.on_import_button_clicked)
        self.remove_subscription.connect("clicked", self.on_remove_button_clicked)
    
    def on_add_button_clicked(self, widget):
        if "/channel/" in self.channel_id_entry.get_text():
            channel_id = self.sub_handler.get_channel_id_from_profile_link(self.channel_id_entry.get_text())
        else:
            channel_id = self.channel_id.get_text()
        channel_id_data = self.sub_handler.get_info_on_channel_id(channel_id)
        if channel_id_data[0]:
            self.subscription_list_store.append([channel_id_data[1], channel_id_data[2]])
            self.channel_id_entry.set_text("")
        else:
            print("ERROR: Could not add channel ID, make sure it exists and its valid.")


    def on_export_button_clicked(self, widget):
        new_sub_list = {}
        for row in self.subscription_list_store:
            new_sub_list[row[0]] = row[1]
        self.sub_handler.export_subscription_list(new_sub_list)

    def on_import_button_clicked(self, widget):
        # dialog = Gtk.FileChooserDialog(title="Please choose your file", add_buttons=Gtk.ButtonsType.OK)
        # response = dialog.run()
        for key, value in self.sub_handler.import_subscription_list().items():
           self.subscription_list_store.append([key, value])
        
    def on_remove_button_clicked(self, widget):
        model, treeiter = self.select.get_selected()
        if treeiter is not None:
            if self.subscription_list_store.remove(treeiter):
                print("Successfully removed row.")
            else:
                print("Failed to remove row.")

win = TreeView()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
