import opml
import requests
from xml.etree import ElementTree

class SubscriptionHandler:
    parsed = None
    base_url = "https://www.youtube.com/feeds/videos.xml?channel_id="
    sub_list = {}

    def import_subscription_list(self):
        self.parsed = opml.parse("youtube_subscriptions")
        for x in range(len(self.parsed[0])): 
            channel_id = None
            if "=" in self.parsed[0][x].xmlUrl[51]:
                channel_id = self.parsed[0][x].xmlUrl[52::]
            self.sub_list[self.parsed[0][x].title] = channel_id
        return self.sub_list

    def export_subscription_list(self, subscription_dictionary):
        if len(subscription_dictionary) == 0:
            return False
        with open("updated_youtube_subscriptions", "w") as file:
            file.write("<opml version=\"1.1\">\n\t<body>\n\t\t")
            file.write("<outline text=\"YouTube Subscriptions\" title=\"YouTube Subscriptions\">\n\t\t")
            for key, value in subscription_dictionary.items():
                xml_line = '<outline text="{0}" title="{0}" type="rss" xmlUrl="{1}" />'.format(key, self.base_url + value)
                file.write(xml_line + "\n\t\t")
            file.write("</outline>\n\t</body>\n</opml>")
        return True

    def get_info_on_channel_id(self, channel_id):
        channel_data = requests.get(self.base_url + channel_id)
        if channel_data.status_code == 200:
            root = ElementTree.fromstring(channel_data.content)
            for child in root:
                if "title" in child.tag:
                    return [True, child.text, channel_id]
        else:
            return [False, "", ""]
    
    def get_channel_id_from_profile_link(self, profile_url):
        r = requests.get(profile_url)
        for line in r.iter_lines():
            if b"https://www.youtube.com/channel/" in line:
                test = line.find(b"https://www.youtube.com/channel/")
                return line[78:102].decode()
        return False

