## YouRSSFeed
This is a program that allows you to modify your local copy of youtube subscriptions.

## Why?
I personally stopped browsing youtube itself and converted to rss feed(s) but that meant I couldnt easily add youtube subscriptions besides editing the given rss file from youtube.
So thats where this program comes in it reads your youtube RSS feed in and you can add new subscriptions to your file without editing it yourself.
